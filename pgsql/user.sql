-- Table: public.u_member

-- DROP TABLE IF EXISTS public.u_member;

CREATE TABLE IF NOT EXISTS public.u_member
(
    u_id integer,
    u_name character varying(10) COLLATE pg_catalog."default",
    u_password text COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.u_member
    OWNER to postgres;