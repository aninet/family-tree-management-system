-- Table: public.family

-- DROP TABLE IF EXISTS public.family;

CREATE TABLE IF NOT EXISTS public.family
(
    id integer NOT NULL,
    f_name character varying(10) COLLATE pg_catalog."default",
    f_sex character varying(5) COLLATE pg_catalog."default",
    birth_date text COLLATE pg_catalog."default",
    die_date text COLLATE pg_catalog."default",
    wedding_date text COLLATE pg_catalog."default",
    couple_name character varying(10) COLLATE pg_catalog."default",
    life text COLLATE pg_catalog."default",
    glory text COLLATE pg_catalog."default",
    CONSTRAINT family_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.family
    OWNER to postgres;