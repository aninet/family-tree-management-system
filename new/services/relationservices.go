package services

import (
	"new/dao"
	"new/models"
)

//显示成员信息
func GetAllRelation() (error, []models.Relation, int64) {
	err, Relationdata, total := dao.GetAllRelation()
	return err, Relationdata, total
}

//删除成员信息
func DelRelation(cno1 int) error {
	err := dao.DelRelation(cno1)
	return err
}

//更改成员信息
func UpdateRelation(Relation models.Relation) error {
	err := dao.UpdateRelation(Relation)
	return err
}

//增加成员信息
func AddRelation(Relation models.Relation) error {
	err := dao.AddRelation(Relation)
	return err
}
