package services

import (
	"new/dao"
	"new/models"
)

//显示成员信息
func GetAllCustomers() (error, []models.Customers, int64) {
	err, Customersdata, total := dao.GetAllCustomers()
	return err, Customersdata, total
}

//删除成员信息
func DelCustomers(id int) error {
	err := dao.DelCustomers(id)
	return err
}

//更改成员信息
func UpdateCustomers(Customers models.Customers) error {
	err := dao.UpdateCustomers(Customers)
	return err
}

//增加成员信息
func AddCustomers(Customers models.Customers) error {
	err := dao.AddCustomers(Customers)
	return err
}
