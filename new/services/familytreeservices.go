package services

import (
	"new/dao"
	"new/models"
)

func GetAllFamilytree() (error, []models.Customers, int64) {
	err, familytreeData, total := dao.GetAllFamilytree()
	return err, familytreeData, total

}
