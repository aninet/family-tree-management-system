package services

import (
	"new/dao"
	"new/models"
)

//显示成员信息
func GetAllResume() (error, []models.Resume, int64) {
	err, Resumedata, total := dao.GetAllResume()
	return err, Resumedata, total
}

//删除成员信息
func DelResume(cno int) error {
	err := dao.DelResume(cno)
	return err
}

//更改成员信息
func UpdateResume(Resume models.Resume) error {
	err := dao.UpdateResume(Resume)
	return err
}

//增加成员信息
func AddResume(Resume models.Resume) error {
	err := dao.AddResume(Resume)
	return err
}
