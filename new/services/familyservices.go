package services

import (
	"new/dao"
	"new/models"
)

//显示成员信息
func GetAllFamily() (error, []models.Family, int64) {
	err, Familydata, total := dao.GetAllFamily()
	return err, Familydata, total
}

//删除成员信息
func DelFamily(fno int) error {
	err := dao.DelFamily(fno)
	return err
}

//更改成员信息
func UpdateFamily(Family models.Family) error {
	err := dao.UpdateFamily(Family)
	return err
}

//增加成员信息
func AddFamily(Family models.Family) error {
	err := dao.AddFamily(Family)
	return err
}
