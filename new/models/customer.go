package models

type Customers struct {
	Id       string `gorm:"column:id"`
	Fno      string `gorm:"column:fno"`
	Name     string `gorm:"column:name"`
	Gender   string `gorm:"column:gender"`
	Birthday string `gorm:"column:birthday"`
	Cdie     string `gorm:"column:cdie"`
	Cwedding string `gorm:"column:cwedding"`
	Fid      string `gorm:"column:fid"`
	Mid      string `gorm:"column:mid"`
	Pids     string `gorm:"column:pids"`
	Img      string `gorm:"column:img"`
	Isdel    string `gorm:"column:is_del"`
}
