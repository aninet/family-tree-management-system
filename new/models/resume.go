package models

type Resume struct {
	ResumeCno     string `gorm:"column:cno"`
	ResumeCname   string `gorm:"column:cname"`
	ResumeLife_id string `gorm:"column:life_id"`
	ResumeLife    string `gorm:"column:life"`
	ResumeTime    string `gorm:"column:time"`
	ResumeHonor   string `gorm:"column:honor"`
}
