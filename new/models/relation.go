package models

type Relation struct {
	RelationCno1   string `gorm:"column:cno1"`
	RelationCname1 string `gorm:"column:cname1"`
	RelationCno2   string `gorm:"column:cno2"`
	RelationCname2 string `gorm:"column:cname2"`
	Relation_Type  string `gorm:"column:relation_type"`
}
