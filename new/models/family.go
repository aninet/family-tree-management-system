package models

type Family struct {
	FamilyFno      string `gorm:"column:fno"`
	FamilyFname    string `gorm:"column:fname"`
	FamilyDescribe string `gorm:"column:describe"`
}
