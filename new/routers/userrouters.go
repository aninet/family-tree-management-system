package routers

import (
	"new/controllers"

	"github.com/gin-gonic/gin"
)

func GetUser(r *gin.RouterGroup) {
	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)
}
