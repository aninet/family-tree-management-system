package routers

import (
	"new/controllers"

	"github.com/gin-gonic/gin"
)

func GetResume(r *gin.RouterGroup) {
	r.GET("/resume", controllers.GetResume)
	r.DELETE("/resume", controllers.DelResume)
	r.PUT("/resume", controllers.UpdateResume)
	r.POST("/resume", controllers.AddResume)

}
