package routers

import (
	"new/middleWare"

	"github.com/gin-gonic/gin"
)

func sysNoCheckRoleRouter(r *gin.RouterGroup) {
	r = r.Group("/apis")
	GetCustomers(r)
	GetRelation(r)
	GetResume(r)
	GetFamily(r)
	GetUser(r)
	GetFamilytree(r)
	// loginRouter(r)
}

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middleWare.Cors())

	g := r.Group("/system")
	sysNoCheckRoleRouter(g)
	return r
}
