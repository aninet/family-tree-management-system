package routers

import (
	"new/controllers"

	"github.com/gin-gonic/gin"
)

func GetRelation(r *gin.RouterGroup) {
	r.GET("/relation", controllers.GetRelation)
	r.DELETE("/relation", controllers.DelRelation)
	r.PUT("/relation", controllers.UpdateRelation)
	r.POST("/relation", controllers.AddRelation)

}
