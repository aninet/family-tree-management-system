package routers

import (
	"github.com/gin-gonic/gin"
	"new/controllers"
)

func GetCustomers(r *gin.RouterGroup){
	r.GET("/customers", controllers.GetCustomers)
	r.DELETE("/customers", controllers.DelCustomers)
	r.PUT("/customers", controllers.UpdateCustomers)
	r.POST("/customers", controllers.AddCustomers)

}