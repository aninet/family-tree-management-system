package routers

import (
	"new/controllers"

	"github.com/gin-gonic/gin"
)

func GetFamilytree(r *gin.RouterGroup) {
	r.GET("/familytree", controllers.GetFamilytree)
}
