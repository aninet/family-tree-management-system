package routers

import (
	"new/controllers"

	"github.com/gin-gonic/gin"
)

func GetFamily(r *gin.RouterGroup) {
	r.GET("/family", controllers.GetFamily)
	r.DELETE("/family", controllers.DelFamily)
	r.PUT("/family", controllers.UpdateFamily)
	r.POST("/family", controllers.AddFamily)

}
