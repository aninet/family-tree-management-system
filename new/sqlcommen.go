package main

import (
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"new/dao"
	"new/kpg/setting"
	"new/routers"
)

func init() {
	setting.Setup()
	dao.Setup()
}


func main() {

	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)
	fmt.Println(endPoint)
	fmt.Println(setting.DatabaseSetting)
	routersInit := routers.InitRouter()
	routersInit.Run(endPoint)
}