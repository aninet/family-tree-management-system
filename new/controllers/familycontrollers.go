package controllers

import (
	"encoding/json"
	"new/kpg/app"
	"new/kpg/e"
	"new/models"
	"new/services"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetFamily(c *gin.Context) {
	err, info, total := services.GetAllFamily()
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
}

// DelCustomers 删除成员
func DelFamily(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	id := com.StrTo(m["FamilyFno"]).MustInt()
	if id == -1 {
		app.INFO(c, 30001, "参数错误")
		return
	}
	err := services.DelFamily(id)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "删除成功")
}

func UpdateFamily(c *gin.Context) {
	b, _ := c.GetRawData() //获取原始字节
	var m map[string]string
	_ = json.Unmarshal(b, &m) //将结构类型转换为json字符串
	if m["FamilyFno"] == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	FamilyFno := m["FamilyFno"]
	FamilyFname := m["FamilyFname"]
	FamilyDescribe := m["FamilyDescribe"]

	if FamilyFno == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	err := services.UpdateFamily(models.Family{FamilyFno, FamilyFname, FamilyDescribe})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "更新成功")
}
func AddFamily(c *gin.Context) {
	b, _ := c.GetRawData() //获取原始字节
	var m map[string]string
	_ = json.Unmarshal(b, &m) //将结构类型转换为json字符串

	FamilyFno := m["FamilyFno"]
	FamilyFname := m["FamilyFname"]
	FamilyDescribe := m["FamilyDescribe"]

	if FamilyFno == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	err := services.AddFamily(models.Family{FamilyFno, FamilyFname, FamilyDescribe})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "添加成功")
}
