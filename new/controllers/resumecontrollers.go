package controllers

import (
	"encoding/json"
	"new/kpg/app"
	"new/kpg/e"
	"new/models"
	"new/services"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetResume(c *gin.Context) {
	err, info, total := services.GetAllResume()
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
}

// DelCustomers 删除成员
func DelResume(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	id := com.StrTo(m["ResumeCno"]).MustInt()
	if id == -1 {
		app.INFO(c, 30001, "参数错误")
		return
	}
	err := services.DelResume(id)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "删除成功")
}

func UpdateResume(c *gin.Context) {
	b, _ := c.GetRawData() //获取原始字节
	var m map[string]string
	_ = json.Unmarshal(b, &m) //将结构类型转换为json字符串
	if m["ResumeCno"] == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	ResumeCno := m["ResumeCno"]
	ResumeCname := m["ResumeCname"]
	ResumeLife_id := m["ResumeLife_id"]
	ResumeLife := m["ResumeLife"]
	ResumeTime := m["ResumeTime"]
	ResumeHonor := m["ResumeHonor"]

	if ResumeCno == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	err := services.UpdateResume(models.Resume{ResumeCno, ResumeCname, ResumeLife_id, ResumeLife, ResumeTime, ResumeHonor})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}

	app.OK(c, map[string]interface{}{}, "更新成功")
}
func AddResume(c *gin.Context) {
	b, _ := c.GetRawData() //获取原始字节
	var m map[string]string
	_ = json.Unmarshal(b, &m) //将结构类型转换为json字符串

	ResumeCno := m["ResumeCno"]
	ResumeCname := m["ResumeCname"]
	ResumeLife_id := m["ResumeLife_id"]
	ResumeTime := m["ResumeTime"]
	ResumeLife := m["ResumeLife"]
	ResumeHonor := m["ResumeHonor"]

	if ResumeCno == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	err := services.AddResume(models.Resume{ResumeCno, ResumeCname, ResumeLife_id, ResumeLife, ResumeTime, ResumeHonor})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "添加成功")
}
