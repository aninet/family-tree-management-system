package controllers

import (
	"encoding/json"
	"new/kpg/app"
	"new/kpg/e"
	"new/models"
	"new/services"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetCustomers(c *gin.Context) {
	// page := -1
	// if arg := c.Query("page"); arg != "" {
	// 	page = com.StrTo(arg).MustInt()
	// }
	// limit := -1
	// if arg := c.Query("limit"); arg != "" {
	// 	limit = com.StrTo(arg).MustInt()
	// }
	// searchId := ""
	// if arg := c.Query("searchId"); arg != "" {
	// 	searchId = arg
	// }
	// supplierParam := map[string]interface{}{
	// 	"page":     page,
	// 	"limit":    limit,
	// 	"searchId": searchId,
	// }
	err, info, total := services.GetAllCustomers()
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
}

// DelCustomers 删除成员
func DelCustomers(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	id := com.StrTo(m["Id"]).MustInt()
	if id == -1 {
		app.INFO(c, 30001, "参数错误")
		return
	}
	err := services.DelCustomers(id)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "删除成功")
}

func UpdateCustomers(c *gin.Context) {
	b, _ := c.GetRawData() //获取原始字节
	var m map[string]string
	_ = json.Unmarshal(b, &m) //将结构类型转换为json字符串
	if m["Id"] == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	Id := m["Id"]
	Fno := m["Fno"]
	Name := m["Name"]
	Gender := m["Gender"]
	Birthday := m["Birthday"]
	Cdie := m["Cdie"]
	Cwedding := m["Cwedding"]
	Mid := m["Mid"]
	Fid := m["Fid"]
	Pids := m["Pids"]
	Img := m["Img"]
	Isdel := m["Isdel"]

	if Id == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	err := services.UpdateCustomers(models.Customers{Id, Fno, Name, Gender, Birthday, Cdie, Cwedding, Mid, Fid, Pids, Img, Isdel})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "更新成功")
}
func AddCustomers(c *gin.Context) {
	b, _ := c.GetRawData() //获取原始字节
	var m map[string]string
	_ = json.Unmarshal(b, &m) //将结构类型转换为json字符串

	Id := m["Id"]
	Fno := m["Fno"]
	Name := m["Name"]
	Gender := m["Gender"]
	Birthday := m["Birthday"]
	Cdie := m["Cdie"]
	Cwedding := m["Cwedding"]
	Mid := m["Mid"]
	Fid := m["Fid"]
	Pids := m["Pids"]
	Img := m["Img"]
	Isdel := m["Isdel"]

	if Id == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	err := services.AddCustomers(models.Customers{Id, Fno, Name, Gender, Birthday, Cdie, Cwedding, Mid, Fid, Pids, Img, Isdel})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "添加成功")
}
