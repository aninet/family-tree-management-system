package controllers

import (
	"new/kpg/app"
	"new/kpg/e"
	"new/services"

	"github.com/gin-gonic/gin"
)

// func GetFamilytree(c *gin.Context) {
// 	err, info, total := services.GetAllFamilytree()
// 	if err != nil {
// 		app.Error(c, e.ERROR, err, err.Error())
// 		return
// 	}
// 	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
// }
func GetFamilytree(c *gin.Context) {
	err, info, total := services.GetAllFamilytree()
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	var m = map[string]interface{}{"value": info, "total": total}
	app.OK(c, m, "查询成功")
}
