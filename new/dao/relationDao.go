package dao

import "new/models"

func GetAllRelation() (error, []models.Relation, int64) {
	var relationData []models.Relation
	//page := customerParam["page"].(int)
	//pageSize := customerParam["limit"].(int)
	//searchName := customerParam["searchName"].(string)
	var total int64
	err := db.Table("relation").Order("cno1 ASC").Count(&total).Find(&relationData).Error
	return err, relationData, total
}
func DelRelation(cno1 int) error {
	var relation []models.Relation
	err := db.Table("relation").Where("cno1 = ?", cno1).Delete(&relation).Error
	return err
}
func UpdateRelation(relation models.Relation) error {
	err := db.Table("relation").Where("cno1 = ?", relation.RelationCno1).Updates(&relation).Error
	return err
}
func AddRelation(relation models.Relation) error {
	err := db.Table("relation").Create(&relation).Error
	return err
}
