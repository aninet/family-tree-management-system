package dao

import "new/models"

func GetAllResume() (error, []models.Resume, int64) {
	var resumeData []models.Resume
	var total int64
	err := db.Table("resume").Order("cno ASC").Count(&total).Find(&resumeData).Error
	return err, resumeData, total
}
func DelResume(cno int) error {
	var resume []models.Resume
	err := db.Table("resume").Where("cno = ?", cno).Delete(&resume).Error
	return err
}
func UpdateResume(resume models.Resume) error {
	err := db.Table("resume").Where("cno = ?", resume.ResumeCno).Updates(&resume).Error
	return err
}
func AddResume(resume models.Resume) error {
	err := db.Table("resume").Create(&resume).Error
	return err
}
