package dao

import "new/models"

func GetAllFamily() (error, []models.Family, int64) {
	var familyData []models.Family
	//page := customerParam["page"].(int)
	//pageSize := customerParam["limit"].(int)
	//searchName := customerParam["searchName"].(string)
	var total int64
	err := db.Table("family").Order("fno ASC").Count(&total).Find(&familyData).Error
	return err, familyData, total
}
func DelFamily(fno int) error {
	var family []models.Family
	err := db.Table("family").Where("fno = ?", fno).Delete(&family).Error
	return err
}
func UpdateFamily(family models.Family) error {
	err := db.Table("family").Where("fno = ?", family.FamilyFno).Updates(&family).Error
	return err
}
func AddFamily(family models.Family) error {
	err := db.Table("family").Create(&family).Error
	return err
}
