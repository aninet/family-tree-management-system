package dao

import "new/models"

func GetAllUser() ([]models.User, int64, error) {
	var userData []models.User
	var total int64
	err := db.Table("user").Count(&total).Find(&userData).Error
	return userData, total, err
}

func DelUser(id int) error {
	var user []models.User
	err := db.Table("user").Where("id = ?", id).Delete(&user).Error
	return err
}

func UpdateUser(user models.User) error {
	err := db.Table("user").Where("id = ?", user.ID).Updates(&user).Error
	return err
}

func AddUser(user models.User) error {
	err := db.Table("user").Create(&user).Error
	return err
}
