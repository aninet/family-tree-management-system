package dao

import "new/models"

func GetAllFamilytree() (error, []models.Customers, int64) {
	var familytreeData []models.Customers
	var total int64

	err := db.Table("customer").Select("id,name,mid,fid,pids,gender,img").Order("id ASC").Count(&total).Find(&familytreeData).Error

	return err, familytreeData, total
}
