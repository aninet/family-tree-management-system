package dao

import "new/models"

func GetAllCustomers() (error, []models.Customers, int64) {
	var customerData []models.Customers
	// page := customer["page"].(int)
	// pageSize := customer["limit"].(int)
	// searchName := customer["searchName"].(string)
	var total int64
	err := db.Table("customer").Order("id ASC").Count(&total).Find(&customerData).Error
	return err, customerData, total
}
func DelCustomers(id int) error {
	var customer []models.Customers
	err := db.Table("customer").Where("id = ? ", id).Delete(&customer).Error
	return err
}
func UpdateCustomers(customer models.Customers) error {
	err := db.Table("customer").Where("id = ?", customer.Id).Updates(&customer).Error
	return err
}
func AddCustomers(customer models.Customers) error {
	err := db.Table("customer").Create(&customer).Error
	return err
}
