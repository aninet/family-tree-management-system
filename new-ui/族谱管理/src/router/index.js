import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    
                    path: '/dashboard',
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Dashboard.vue'),
                    meta: { title: '系统首页' }
                },
                {

                    path: '/table',
                    component: () => import(/* webpackChunkName: "icon" */ '../components/page/family.vue'),
                    meta: { title: '家族表' }
                },
                {

                    path: '/icon',
                    component: () => import(/* webpackChunkName: "icon" */ '../components/page/customer.vue'),
                    meta: { title: '家族成员信息' }
                },
                {

                    path: '/message',
                    component: () => import(/* webpackChunkName: "icon" */ '../components/page/resume.vue'),
                    meta: { title: '成员履历' }
                },
                {

                    path: '/relation',
                    component: () => import(/* webpackChunkName: "icon" */ '../components/page/relation.vue'),
                    meta: { title: '成员关系表' }
                },
                {
                    path: '/upload',
                    component: () => import(/* webpackChunkName: "upload" */ '../components/page/repairlista.vue'),
                    meta: { title: '可视化图' }
                },
                {
                    path: '/tree',
                    component: () => import(/* webpackChunkName: "upload" */ '../components/page/familytree.vue'),
                    meta: { title: '家谱' }
                },
            ]
        },
        {
            path: '/login',
            component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
