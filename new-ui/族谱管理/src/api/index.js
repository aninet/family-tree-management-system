import request from '../utils/request';




export const fetchgetcustomer = query => {
    return request({
        url: '/system/apis/customers',
        method: 'get',
        params: query
    });
};
export const fetchupdatecustomer = query => {
    return request({
        url: '/system/apis/customers',
        method: 'put',
        data: query
    });
};

export const customerdelete = query => {
    return request({
        url: '/system/apis/customers',
        method: 'delete',
        data: query
    });
};
export const fetchaddcustomer = query => {
    return request({
        url: '/system/apis/customers',
        method: 'post',
        data: query
    });
};

export const fetchgetrelation = query => {
    return request({
        url: '/system/apis/relation',
        method: 'get',
        params: query
    });
};
export const fetchupdaterelation = query => {
    return request({
        url: '/system/apis/relation',
        method: 'put',
        data: query
    });
};

export const relationdelete = query => {
    return request({
        url: '/system/apis/relation',
        method: 'delete',
        data: query
    });
};
export const fetchaddrelation = query => {
    return request({
        url: '/system/apis/relation',
        method: 'post',
        data: query
    });
};
export const fetchgetfamily = query => {
    return request({
        url: '/system/apis/family',
        method: 'get',
        params: query
    });
};
export const fetchupdatefamily = query => {
    return request({
        url: '/system/apis/family',
        method: 'put',
        data: query
    });
};

export const familydelete = query => {
    return request({
        url: '/system/apis/family',
        method: 'delete',
        data: query
    });
};
export const fetchaddfamily = query => {
    return request({
        url: '/system/apis/family',
        method: 'post',
        data: query
    });
};
export const fetchgetresume = query => {
    return request({
        url: '/system/apis/resume',
        method: 'get',
        params: query
    });
};
export const fetchupdateresume = query => {
    return request({
        url: '/system/apis/resume',
        method: 'put',
        data: query
    });
};

export const resumedelete = query => {
    return request({
        url: '/system/apis/resume',
        method: 'delete',
        data: query
    });
};
export const fetchaddresume = query => {
    return request({
        url: '/system/apis/resume',
        method: 'post',
        data: query
    });
};
